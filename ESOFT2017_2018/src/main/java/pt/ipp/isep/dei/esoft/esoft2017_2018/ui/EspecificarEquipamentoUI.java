/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import pt.ipp.isep.dei.esoft.esoft2017_2018.controller.EspecificarEquipamentoController;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarEquipamentoUI
{
    private Empresa m_oEmpresa;
    private EspecificarEquipamentoController m_controller;

    public EspecificarEquipamentoUI( Empresa oEmpresa )
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarEquipamentoController(oEmpresa);
    }

    public void run()
    {
        System.out.println("\nNovo Equipamento:");
        m_controller.novoEquipamento();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Equipamento? (S/N)")) {
            if (m_controller.registaEquipamento()) {
                System.out.println("Equipamento registado.");
            } else {
                System.out.println("Equipamento não registado.");
            }
        }
    }
    
    private void introduzDados() {
        String sDescricao = Utils.readLineFromConsole("Introduza Descricão: ");
        String sEndLogico = Utils.readLineFromConsole("Introduza Endereço Lógico: ");
        String sEndFisico = Utils.readLineFromConsole("Introduza Endereço Fisico: ");
        String sFicheiro = Utils.readLineFromConsole("Introduza Ficheiro Configuração: ");
       
        m_controller.setDados(sDescricao, sEndLogico, sEndFisico, sFicheiro);
        m_controller.validaEquipamento();
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nEquipamento:\n" + m_controller.getEquipamentoString());
    }
 
}
